import tempfile

from prefixcommand import BasicEnv, PrefixCmdEnv, in_dir


def test_communicate():
    env = BasicEnv()
    child = env.cmd(["sh", "-c", "echo hello"])
    assert child.stdout, "hello\n"
    child2 = env.make_readable().read_cmd(["sh", "-c", "echo hello"])
    assert child2.stdout, "hello\n"


def test_read_cmd():
    env = BasicEnv()
    child = env.make_readable().read_cmd(["sh", "-c", "echo hello"])
    assert child.stdout, "hello\n"


def test_prefix_command():
    with tempfile.TemporaryDirectory() as dir_:
        env = PrefixCmdEnv(in_dir(dir_), BasicEnv())
        child = env.cmd(["pwd"])
        assert child.stdout.endswith((dir_ + "\n").encode())
