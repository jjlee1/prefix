`prefixcommand` is a Python package for working with prefix commands and "command environments" and related concepts

# Concepts

An "environment" or "env" (`prefixcommand.Env`, from command.py) does not refer to the Unix process environment containing environment variables.
Where literal Unix environment variables are meant, "environ" is used instead (e.g. see `compose_environ` below).
An env is instead just some abstract context which you can use to run commands using the `cmd` method.
The most used are:

- `prefixcommand.BasicEnv`: running a command in this environment just runs it.
- `prefixcommand.PrefixCmdEnv`: running a command in this environment runs it with a prefix command prepended
  For example `PrefixCmdEnv(["sudo"], BasicEnv())` will run commands under `sudo`.

So, the "abstract context" is usually concretely just a prefix command like `sudo`,
implemented using `PrefixCmdEnv`.

A environment wrapper (`prefixcommand.Wrapper`) is a callable that takes an `Env` as its only argument and returns another.
For example:

- `prefixcommand.VerboseWrapper` is a class implementing the `Env` interface,
 whose constructor takes an arbitrary `Env` (so the constructor has type `prefixcommand.Wrapper`,
 i.e. `Callable[[prefixcommand.Env], prefixcommand.Env]`).
 Its `cmd` method prints the command before running it in the wrapped environment.
 This is used to implement the `-v|--verbose` command line switch provided by `add_basic_env_arguments` and `get_env_from_arguments`.


- `prefixcommand.NullWrapper` is a class that doesn't actually run the command passed to `cmd`.
 This is used to implement the `-n|--dry-run` command line switch provided by `add_basic_env_arguments` and `get_env_from_arguments`.

`PrefixCmdEnv` is not itself a wrapper even though it does wrap an `Env`,
because it takes two arguments, not one.
You can make a `Wrapper` for a prefix command by partially applying `PrefixCmdEnv` e.g. using `prefix_wrapper`.
This is a common pattern.

## The `read_cmd` method

`.read_cmd` is just like `.cmd` -- it runs a command.
This is a method on `ReadableEnvProtocol`.

Technically, the only difference is that `ReadableEnv`, which implements the method, has two environments:
a "read" one that will always actually run its commands,
and a "write" one that will do nothing if you passed a "dry-run" env.
When you use `.cmd`, it always uses the "write" env.
When you use `.read_cmd`, it always uses the "read" env.

You should run all commands that might have side effects using `.cmd`,
and all other commands using `.read_cmd` (those commands should instead return data -- e.g. using `cat` to read file contents).

This "command-query separation" allows to fetch information safely by means of running commands,
even when using the `-n` (`--dry-run`) switch.

## Why commands?

Why might you want to use commands rather than Python code for reading information with `.read_cmd`?
The same reasons you might want to use commands for causing side-effects in a build system:

- Build and development functionality is often available using commands
- It can result in a uniform implementation ("everything is a command")
- It provides a means of implementing the `--dry-run` and `--verbose` switches without adding any extra code

## Why prefix commands?

- They compose well
- Provides a way to separate commands from environments to run them in (e.g. run in docker or run in a Python virtualenv)

The context for `prefixcommand` is build systems and other tools that make heavy use of commands
(for an example of a non-build tool, see [`git-meld-index`](https://github.com/jjlee/git-meld-index)),
but here is a reason to use prefix commands in your interactive shell
(see for example the commands in [`bash_prefix_commands`](https://gitlab.com/jjlee1/bash-prefix-commands)):

- They provide full context in your shell history if you ever forgot things like "which directory do I need to run this in?"
